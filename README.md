# Scala/Java - Maven
Konfiguracja repozytorium:
- stworzyc plik: ~/.m2/settings.xml
umiescic w nim:
```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                          https://maven.apache.org/xsd/settings-1.0.0.xsd">
    <servers>
        <server>
            <id>poc--POC_repository</id>
            <username>aws</username>
            <password>${env.CODEARTIFACT_AUTH_TOKEN}</password>
        </server>
    </servers>
</settings>
```


```bash
export AWS_PROFILE=dev
export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain poc --domain-owner 686422572251 --query authorizationToken --output text`
```

# NPM

```bash
git checkout origin/master package.json
git checkout origin/master package-lock.json

rm -rf node_modules
```

```bash
export AWS_PROFILE=dev

aws codeartifact login --tool npm --domain poc --domain-owner 686422572251 --repository POC_repository

npm install
```

# Python requirements



```bash
# https://tcl-research.atlassian.net/wiki/spaces/CLOUD/pages/1876623361/Python+Repository
export AWS_PROFILE=dev
export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain tcl-research-poland --domain-owner 686422572251 --query authorizationToken --output text`
pip config set global.index-url https://aws:$CODEARTIFACT_AUTH_TOKEN@tcl-research-poland-686422572251.d.codeartifact.eu-west-1.amazonaws.com/pypi/cloud-team/simple/
pip install -r requirements.txt
```

# Typescript

when writing infrastructure tests you can print the expected stack by using:
```bash
expectCDK(stack).to(matchTemplate({}));
```

cdk testking
add this line to your test and compare the json
```typescript
expectCDK(stack).to(matchTemplate({}))
```

