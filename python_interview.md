```python
def add(a, b):
    return a + b


def subtract(a, b):
    return a - b


if __name__ == '__main__':
    a, b = 4, 5
    print((subtract if a > b else add)(a, b))
```

```python
>>> def func(*args, **kwargs): 
>>>    print(args) 
>>>    print(kwargs) 
>>>
>>> func()
________________?

>>> func(1, a=2)
________________?
>>> func(a=2)
________________?
>>> func(a=2, 1)
________________?

```


```python
>>> a = [1, 2, 3]
>>> a[::-1]
?
>>> a[::2]
?
# advanced
>>> a[-1:2:-1] 
?
```

```python
latest = None
for obj in bucket.objects.filter(Prefix=prefix):
     if latest is None:
         latest = obj
         continue
     if latest.last_modified <= obj.last_modified:
         latest = obj
latest
# - jak napisac krocej (jednolinijkowiec)
# latest = max(bucket.objects.filter(Prefix=prefix), key=lambda obj: obj.last_modified)
```

```python
>>> class Dog:
...    def __init__(self, name):
...       self.name = name
... 
>>> pies1 = Dog('Reksio')
>>> pies2 = Dog('Reksio')
>>> pies1 == pies2
?
```

inne pytania otwarte:
- co to dekorator w pythonie
- list comprehension
    - co sie stanie jak [] zmeinimy na () 
        - co to generatory, kiedy sie ich uzywa, jakie sa typy generatorow
- jak dbasz o jakosc oprogramowania- co jest wazne z Twojej perspektywy
- co to pep8, jakie zasady znasz
- jakie znasz typy testow, jakie implementujesz day2day
- jak zamodelujesz obiekt domenowy- np. person (chodzi o podanie, ze obiekt moze byc dictionary, named tuple, data class etc.)
- jakie znasz typy deploymentu


pytania pythonowe, ktorych jeszcze nie zadalem
```python
if name == 'main':
    if (True and False):
        print('a')
    elif(False or True and False):
        print('d')
    elif(True or True and False):
        print('b')
    else:
        print('f')
```

```python
# What will this print?
False == False in [False]
```

```python
def some_method_a():
    print('I am inside of the method a')
    return True


def some_method_b():
    print('I am inside of the method b')
    return False


if name == 'main':
    if some_method_a() or some_method_b():
        print('inside if statement')
    else:
        print('inside else statement')
```

other questions:
- How does break, continue and pass work?
- can assertions be used in the case of a production code?

advanced questions:
- what are python [slots](http://tech.oyster.com/save-ram-with-python-slots/)
- what is GIL in python
- what is an async keyword, when it should be used
